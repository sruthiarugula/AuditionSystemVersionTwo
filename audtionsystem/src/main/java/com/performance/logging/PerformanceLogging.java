package com.performance.logging;

import org.apache.log4j.Logger;
import org.apache.log4j.Priority;

import com.performer.audtionsystem.Audition;

public class PerformanceLogging {

	final static Logger logger = Logger.getLogger(PerformanceLogging.class);

	public void logPerformance() {

		Audition audition = new Audition();

		// logger.lo(audition.audtionDay());

		logger.log(Priority.INFO, audition.audtionDay());

	}
}