package com.performer.audtionsystem;

class Vocalist extends Performers {
	private String key;

	private int vocalistId;
	private int volume;

	public Vocalist(String key, int vocalistId) {
		super();
		this.key = key;
		this.vocalistId = vocalistId;
	}

	public Vocalist(String key, int vocalistId, int volume) {
		super();
		this.key = key;
		this.vocalistId = vocalistId;
		this.volume = volume;
	}

	@Override
	String givePerformance() throws IllegalArgumentException {
		String value = null;
		if (this.volume == 0) {
			// I sing in the key of – G – at the volume 5 - 1245 ”
			value = "I sing in the key of" + " " + "-" + " " + this.key + " " + "-" + " " + this.vocalistId;

		} else if (this.volume >= 1 && this.volume <= 10) {
			// I sing in the key of – G – at the volume 5 - 1245 ”
			value = "I sing in the key of" + " " + "-" + " " + this.key + " " + "-" + " " + "at the volume "
					+ this.volume + " " + "-" + " " + this.vocalistId;

		}

		if (this.volume < 0 || this.volume > 10) {
			throw new IllegalArgumentException("Volume not acceptable");
		}
		return value;
	}
}
