package com.performer.audtionsystem;

class Dancer extends Performers {

	private int dancerId;
	private String danceStyle;//No setter for style of dance-cant be altered
	private final String typeOfPerformer = "dancer";

	public int getDancerId() {
		return dancerId;
	}

	public void setDancerId(int dancerId) {
		this.dancerId = dancerId;
	}

	public String getDanceStyle() {
		return danceStyle;
	}

	public String getTypeOfPerformer() {
		return typeOfPerformer;
	}

	public Dancer(int dancerId, String danceStyle) {
		super();
		this.dancerId = dancerId;
		this.danceStyle = danceStyle;

	}

	@Override
	String givePerformance() {
		// TODO Auto-generated method stub

		String value = this.danceStyle + " " + "-" + " " + this.dancerId + " " + "-" + " " + this.typeOfPerformer;
		return value;
	}

}
