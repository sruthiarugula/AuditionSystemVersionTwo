package com.performer.audtionsystem;

import static org.junit.Assert.*;

import org.junit.Test;

public class DancerTest {
	Performers performer1;

	@Test

	public void dancerPerformanceTest() {
		// TODO Auto-generated method stub

		 performer1 = new Dancer(130,"tap");

		String expected = "tap - 130 - dancer";
		String actual = performer1.givePerformance();
		assertEquals(expected, actual);

	}
	@Test
	public void dancerPerformanceNegativeTest() {
		// TODO Auto-generated method stub

		performer1 = new Dancer(130,"tap");

		String expected = "tap-130 - dancer";
		String actual = performer1.givePerformance();
		assertEquals(expected, actual);

	}
	@Test
	public void dancerPerformanceNegativeSecondTest() {
		// TODO Auto-generated method stub

		performer1 = new Dancer(130,"tap");

		String expected = "tap130 - dancer";
		String actual = performer1.givePerformance();
		assertEquals(expected, actual);

	}
	@Test
	public void dancerPerformanceNegativeThirdTest() {
		// TODO Auto-generated method stub

		performer1 = new Dancer(130,"tap");

		String expected = "tap130dancer";
		String actual = performer1.givePerformance();
		assertEquals(expected, actual);

	}
	public void dancerPerformanceNegativeFourthTest() {
		// TODO Auto-generated method stub

		performer1 = new Dancer(130,"tap");

		String expected = "tap130-dancer";
		String actual = performer1.givePerformance();
		assertEquals(expected, actual);

	}
}
