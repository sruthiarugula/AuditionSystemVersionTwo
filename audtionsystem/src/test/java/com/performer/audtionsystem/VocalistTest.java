package com.performer.audtionsystem;

import static org.junit.Assert.*;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class VocalistTest {
	Performers performer1;

	@Test

	public void vocalistPerformanceTest() {
		// TODO Auto-generated method stub

		performer1 = new Vocalist("G", 1191);

		String expected = "I sing in the key of - G - 1191";
		String actual = performer1.givePerformance();
		assertEquals(expected, actual);

	}

	@Test
	public void vocalistPerformanceNegativeTest() {
		// TODO Auto-generated method stub

		performer1 = new Vocalist("G", 1191);

		String expected = "I sing in the key ofG - 1191";
		String actual = performer1.givePerformance();
		assertEquals(expected, actual);

	}

	@Test
	public void vocalistPerformanceVolumeTest() {
		// TODO Auto-generated method stub

		performer1 = new Vocalist("G", 1245, 10);

		String expected = "I sing in the key of - G - at the volume 10 - 1245";
		String actual = performer1.givePerformance();
		assertEquals(expected, actual);

	}

	@Test
	public void vocalistPerformanceVolumeNegativeTest() {
		// TODO Auto-generated method stub

		performer1 = new Vocalist("G", 1245, 10);

		String expected = "I sing in the key of G - at the volume 10 - 1245";
		String actual = performer1.givePerformance();
		assertEquals(expected, actual);

	}
	

	@Rule
	public ExpectedException anException = ExpectedException.none();

	@Test
	public void vocalistPerformanceOutOfRangeVolumeTest() {
		// TODO Auto-generated method stub
		anException.expect(IllegalArgumentException.class);
		anException.expectMessage("Volume not acceptable");
		performer1 = new Vocalist("G", 1245, 9);

		performer1.givePerformance();

	}
}
