package com.performer.audtionsystem;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PerformerTest {
	
	

	@Before

	public void doSetUp() {

	}

	@Test

	public void performerPerformanceTest() {
		// TODO Auto-generated method stub

		Performers performer1 = new OtherPerformer(124);

		String expected = "124 - performer";
		String actual = performer1.givePerformance();
		assertEquals(expected, actual);

	}
	@Test
	public void performerPerformanceNegativeTest() {
		// TODO Auto-generated method stub

		Performers performer1 = new OtherPerformer(125);

		String expected = "125-performer";
		String actual = performer1.givePerformance();
		assertEquals(expected, actual);

	}

}
